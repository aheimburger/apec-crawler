FROM golang:1.9

WORKDIR /go/src/clustree.com/backend/apec_crawler.git

RUN curl https://glide.sh/get | sh
RUN go get -u github.com/golang/lint/golint
