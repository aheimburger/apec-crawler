ROOT_DIR :=$(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))

# extract 'std' from 'go-std-lib', or 'std' from 'std.git'
NAMEREGEXP ?= s/([a-z]+-)?([a-z]+)(-[a-z]+)?(.git)?/\2/
NAME ?= $(shell echo $(BASENAME) | sed -E "$(NAMEREGEXP)")
BASENAME ?= $(shell basename $(PWD))
CONTAINER ?= $(NAME)
export USER_ID ?= $(shell id -u)

BUILDDIR ?= build
VENDORDIR ?= vendor
BINDIR ?= $(BUILDDIR)/bin
BINPATH ?= $(BINDIR)/$(NAME)

BASH ?= /usr/bin/env bash
BASHFLAGS ?=
DOCKER ?= docker
DOCKERCOMPOSE ?= docker-compose
DOCKERCOMPOSEDOWNFLAGS ?= --volumes --remove-orphans
DOCKERCOMPOSEPULLFLAGS ?=
DOCKERCOMPOSERUNFLAGS ?= --service-ports --rm --no-deps
DOCKERCOMPOSEUPFLAGS ?=
EXEC ?= $(BASH) $(BASHFLAGS) -c

CI_BUILD_TAG ?= $(shell git rev-parse --verify HEAD)

# not inside a docker container, wrap the binaries calls with docker compose
ifeq ($(wildcard /.dockerenv),)
    EXEC = $(DOCKERCOMPOSE) run $(DOCKERCOMPOSERUNFLAGS) $(CONTAINER) $(BASH) $(BASHFLAGS) -c
endif

export PKG ?= clustree.com/backend/$(NAME).git

# list directories containing main files, not in vendor, at max depth 2
BINARYPKG_LIST ?= $(shell find . -maxdepth 3 -name 'main.go' | xargs -I {} dirname {} | grep -v vendor | sort -u)
# list directories containing go files, not in vendor, at max depth 2
PKG_LIST ?= $(shell find . -maxdepth 3 -name '*.go' | xargs -I {} dirname {} | grep -v vendor | sort -u)

PKG_VERSION ?= $(shell git describe --tags)

GO ?= go
GOBUILD ?= $(GO) build
GOBUILDFLAGS ?= -ldflags \"-w -s -X main.Version=${PKG_VERSION}\" -gcflags \"-trimpath \$${GOPATH}/src\"
GOLINTER ?= gometalinter
GOLINTERFLAGS ?= \
        --concurrency=4 \
        --deadline=1m \
        --disable-all \
        --enable=deadcode \
        --enable=errcheck \
        --enable=goconst \
        --enable=gofmt \
        --enable=goimports \
        --enable=golint \
        --enable=gosimple \
        --enable=ineffassign \
        --enable=interfacer \
        --enable=misspell \
        --enable=staticcheck \
        --enable=structcheck \
        --enable=unconvert \
        --enable=unused \
        --enable=varcheck \
        --enable=vet \
        --enable=vetshadow \
        --exclude='main.go:' \
        --exclude='.*_easyjson.go' \
        --exclude='.*_bindata.go' \
        --exclude='.*_test\.go:.*error return value not checked.*\(errcheck\)$$' \
	--exclude='warning:*\(gas\)$$' \
        --exclude='Errors unhandled.,LOW,HIGH \(gas\)$$' \
        --exclude='Use of unsafe calls should be audited,LOW,HIGH \(gas\)$$' \
        --exclude='^/usr/local/go' \
        --exclude='declaration of \"err\" shadows declaration at.*\(vetshadow\)$$' \
        --exclude='duplicate of.*_pool.go.*\(dupl\)$$' \
        --exclude='duplicate of.*_test.go.*\(dupl\)$$' \
        --exclude='duplicate of.config.go.*\(dupl\)$$' \
        --exclude='error return value not checked.*(Close|Log|Read|Run|Write|Print|io.Copy\(ioutil.Discard).*\(errcheck\)$$' \
        --exclude='SQL string formatting' \
	--exclude='Potential hardcoded credentials' \
        --tests \
        --vendor
GOPKGMANAGER ?= glide
GOPKGMANAGERINSTALL ?= install
GOPKGMANAGERUPDATE ?= update

.PHONY: clean
clean:: ## remove project's dependencies, cache, binaries and ci artifacts
	rm -rf $(BUILDDIR)
	rm -rf $(VENDORDIR)

.PHONY: help
help:: ## print this message
	@grep -E '^[a-zA-Z_-]+::.*?## .*$$' $(word $(words $(MAKEFILE_LIST)),$(MAKEFILE_LIST)) | sort | awk 'BEGIN {FS = "::.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY: pull
pull:: ## pull docker composition images
	@$(BASH) $(BASHFLAGS) -c "[ ! -f /.dockerenv ] || { echo 'error: cannot pull docker composition images from inside a container'; exit 1; }"
	$(DOCKERCOMPOSE) pull $(DOCKERCOMPOSEPULLFLAGS)

.PHONY: shell
shell:: DOCKERCOMPOSERUNFLAGS = --service-ports --rm
shell:: ## run project's shell
	$(EXEC) "$(BASH)"

.PHONY: up
up:: ## start up docker composition
	@$(BASH) $(BASHFLAGS) -c "[ ! -f /.dockerenv ] || { echo 'error: cannot up docker composition from inside a container'; exit 1; }"
	$(DOCKERCOMPOSE) up $(DOCKERCOMPOSEUPFLAGS)

.PHONY: down
down:: ## shut down docker composition
	@$(BASH) $(BASHFLAGS) -c "[ ! -f /.dockerenv ] || { echo 'error: cannot shut down docker composition from inside a container'; exit 1; }"
	$(DOCKERCOMPOSE) down $(DOCKERCOMPOSEDOWNFLAGS)

.PHONY: build
build:: ## build the project
	@rm -rf $(BUILDDIR) && mkdir -p $(BUILDDIR)
	$(EXEC) "\
	for pkg in $(BINARYPKG_LIST); do \
		pkgname=\$${pkg//\/bin/} ; \
		$(GOBUILD) $(GOBUILDFLAGS) -o $(BINDIR)/\$${pkgname} \$${pkg}; \
	done; \
	"

.PHONY: run
run::
	$(EXEC) "build/bin/$(exec)"

.PHONY: install-dependencies
install-dependencies:: ## install project's dependencies using lock file
	$(EXEC) "$(GOPKGMANAGER) $(GOPKGMANAGERINSTALL)"

.PHONY: update-dependencies
update-dependencies:: ## update project's dependencies ignoring lock file
	$(EXEC) "$(GOPKGMANAGER) $(GOPKGMANAGERUPDATE)"

.PHONY: lint
lint::  ## lint project's code
	$(EXEC) "$(GOLINTER) $(GOLINTERFLAGS) $(PKG_LIST)"
