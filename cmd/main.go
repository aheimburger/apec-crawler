package main

import (
	"fmt"
	"net/http"
	"regexp"
	"strings"
	"time"

	"github.com/PuerkitoBio/gocrawl"
	"github.com/PuerkitoBio/goquery"
	"github.com/kennygrant/sanitize"
)

type Ext struct {
	*gocrawl.DefaultExtender
}

var rxOk = regexp.MustCompile(`Fiches-metiers\/Metiers-Par-Categories$`)
var rxOk1 = regexp.MustCompile(`Fiches-metiers\/Metiers-Par-Categories\/[A-z0-9\-]+`)
var rxOk2 = regexp.MustCompile(`Fiches-metiers\/Metiers-Par-Categories\/[A-z0-9\-]+\/[A-z0-9\-]+$`)

func (e *Ext) Visit(ctx *gocrawl.URLContext, res *http.Response, doc *goquery.Document) (interface{}, bool) {
	if rxOk2.MatchString(ctx.NormalizedURL().String()) == false {
		return nil, true
	}
	article := doc.Find("div.article")
	fmt.Printf("Job title : %s\n ", getFunctionTitle(article))
	fmt.Printf("\turl : %v\n ", ctx.NormalizedURL().String())
	fmt.Printf("\tsynonyms : %v\n ", strings.Join(getListAfterTitle(article, 3, "Autres intitulés"), ", "))
	fmt.Printf("\tactivities : %v\n ", getActivities(article))
	fmt.Printf("\thard skills : %v\n ", strings.Join(getListAfterTitle(article, 3, "Compétences techniques"), ", "))
	fmt.Printf("\tsoft skills : %v\n ", strings.Join(getListAfterTitle(article, 3, "Aptitudes professionnelles"), ", "))
	fmt.Printf("\tdiplomes : %v\n ", strings.Join(getListAfterTitle(article, 3, "Diplômes requis"), ", "))
	fmt.Printf("\tprevious pos : %v\n ", strings.Join(getListAfterTitle(article, 3, "Postes précédents"), ", "))
	fmt.Printf("\twork with internally : %v\n ", strings.Join(getListAfterTitle(article, 3, "Internes"), ", "))
	fmt.Printf("\twork with externally : %v\n ", strings.Join(getListAfterTitle(article, 3, "Externes"), ", "))
	fmt.Printf("\tboss: %v\n ", strings.Join(getListAfterTitle(article, 2, "Rattachement hiérarchique"), ", "))
	fmt.Printf("\tnext pos: %v\n ", strings.Join(getListAfterTitle(article, 2, "Evolution professionnelle"), ", "))

	return nil, true
}

func (e *Ext) Filter(ctx *gocrawl.URLContext, isVisited bool) bool {
	url := ctx.NormalizedURL().String()
	return !isVisited && (rxOk.MatchString(url) || rxOk1.MatchString(url))
}

func getFunctionTitle(root *goquery.Selection) string {
	title := ""
	if root != nil {
		title = root.Find("h1").First().Text()
	}
	return clean(title)
}

func getActivities(root *goquery.Selection) string {
	html, _ := root.Html()
	raw := strings.Replace(html, "</h3>", "  ", -1)
	raw = sanitize.HTML(raw)
	i0 := strings.Index(raw, "Activités principales")
	i0 += len("Activités principales")
	i1 := strings.Index(raw, "Activités éventuelles")
	return raw[i0:i1]
}

func getListAfterTitle(root *goquery.Selection, z int, title string) []string {
	var c []string
	cssSelector := fmt.Sprintf("h%d:contains('%s') + ul li", z, title)
	root.Find(cssSelector).Each(func(i int, s *goquery.Selection) {
		c = append(c, clean(s.Text()))
	})
	return c
}

func clean(s string) string {
	return strings.Trim(sanitize.HTML(s), "\n")
}

func main() {
	ext := &Ext{&gocrawl.DefaultExtender{}}
	// Set custom options
	opts := gocrawl.NewOptions(ext)
	opts.CrawlDelay = 1 * time.Second
	opts.LogFlags = gocrawl.LogError
	opts.SameHostOnly = false
	opts.MaxVisits = 1

	c := gocrawl.NewCrawlerWithOptions(opts)
	c.Run("https://cadres.apec.fr/Emploi/Marche-Emploi/Fiches-Apec/Fiches-metiers/Metiers-Par-Categories/Gestion-finance-administration/Banquier-Conseil")
}
